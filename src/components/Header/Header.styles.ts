import styled from 'styled-components/native';

export const Container = styled.View`
  height: 80px;
  width: 100%;

  padding-top: 15px;

  background-color: #476A6F;

  align-items: center;
  justify-content: center;

  border: 0px solid #000000;
  border-bottom-width: 1px;
`;

export const Text = styled.Text`
  color: #FFFFFF;
  font-size: 24px;
  font-family: 'RobotoMono_400Regular';
`;
